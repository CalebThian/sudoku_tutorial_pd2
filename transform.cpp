#include <iostream>
#include "sudoku.h"
using namespace std;


int main(){
	int su[9][9];
	for(int i=0;i<9;i++){
		for(int j=0;j<9;j++)
			cin>>su[i][j];
	}

	Sudoku S(su);
	S.pSudoku();
	while (1){
		int ins;
		cin>>ins;
		if(ins==1){
			int x;
			int y;
			cin>>x>>y;
			S.swapNum(x,y);
		}else if(ins == 2){
			int x;
			int y;
			cin>>x>>y;
			S.swapRow(x*3,y*3);
			S.swapRow(x*3+1,y*3+1);
			S.swapRow(x*3+2,y*3+2);
		}else if(ins == 3){
			int x;
			int y;
			cin>>x>>y;
			S.swapCol(x*3,y*3);
			S.swapCol(x*3+1,y*3+1);
			S.swapCol(x*3+2,y*3+2);
		}else if(ins == 4){
			int x;
			cin>>x;
			S.rotate(x);
		}else if(ins == 5){
			int x;
			cin>>x;
			S.flip(x);
		}else if(ins == 0){
			break;
		}
	}
		S.pSudoku();
	return 0;
}
