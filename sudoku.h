#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>
using namespace std;

class Sudoku{

	public:
		Sudoku();//ctor()
		Sudoku(int s[9][9]);
	
		void set_s(int s[9][9]);

		//generate
		void generate();

		void swapNum(int x,int y);//x->y ;y->x
		void swapRow(int x,int y);
		void swapCol(int x,int y);
		void rotate(int x);
		void flip(int x);
		void empty(int x,int y);//Empty (x,y),i.e. (x,y)=0;
		


		void pSudoku();//print out the Sudoku;
		void pUnique();//print out the unique solution if have;

		void solve(int x,int y);//Solve (x,y)	

		void next(int i,int j);
		
		bool can_fill(int x,int y,int k);//(x,y)=k?
		bool can_Row(int x,int k);
		bool can_Col(int y,int k);
		bool can_Cell(int x,int y,int k);


	private:

		//Your Sudoku 
		int sudoku[9][9]={{1,2,3,4,5,6,7,8,9},
						  {4,5,6,7,8,9,1,2,3},
						  {7,8,9,1,2,3,4,5,6},
					 	  {2,1,4,3,6,5,8,9,7},
					 	  {3,6,5,8,9,7,2,1,4},
					  	  {8,9,7,2,1,4,3,6,5},
					      {5,3,1,6,4,2,9,7,8},
					      {6,4,2,9,7,8,5,3,1},
					      {9,7,8,5,3,1,6,4,2}
					      };
		//int sudoku[9][9];
		int qty_s=0;

		int unique[9][9];
};

#endif 

