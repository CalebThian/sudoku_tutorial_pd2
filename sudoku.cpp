#include <iostream>
#include "sudoku.h"
#include <ctime>
#include <cstdlib>
using namespace std;

Sudoku::Sudoku(){
	srand(time(NULL));//Set the seed of rand()	
/*	sudoku[9][9]={{1,2,3,4,5,6,7,8,9},
						  {4,5,6,7,8,9,1,2,3},
						  {7,8,9,1,2,3,4,5,6},
					 	  {2,1,4,3,6,5,8,9,7},
					 	  {3,6,5,8,9,7,2,1,4},
					  	  {8,9,7,2,1,4,3,6,5},
					      {5,3,1,6,4,2,9,7,8},
					      {6,4,2,9,7,8,5,3,1},
					      {9,7,8,5,3,1,6,4,2}
					      };*/
    
}

Sudoku::Sudoku(int s[9][9]){
	for(int i=0;i<9;++i){
		for(int j=0;j<9;++j)
			sudoku[i][j]=s[i][j];
	}
}

void Sudoku::set_s(int s[9][9]){
	for(int i=0;i<9;++i){
		for(int j=0;j<9;++j)
			sudoku[i][j]=s[i][j];
	}
}


void Sudoku::generate(){
	/*//Execute void function

	//swapNum
	int times=rand()%100;//execute " " swapNum()
	for(int i=0;i<times;++i){
		int x=rand()%9+1;
		int y=rand()%9+1;
		this->swapNum(x,y);
	}

	//swapRow
	times = rand()%75;
	for(int i=0;i<times;++i){
		int t=rand()%3*3;
		int x = rand()%3+t;
		int y= rand()%3+t;
		this->swapRow(x,y);
	}

	//swapCol
	times = rand()%99;
	for(int i=0;i<times;++i){
		int t=rand()%3*3;
		int x=rand()%3+t;
		int y=rand()%3+t;
		this->swapCol(x,y);
	}*/


	//this->rotate(rand()%4);

	//this->flip(rand()%1);

	/*Another kind of rand() method*/
	
		int	times=rand()%10000+10000;
	 	for(int i=0;i<times;++i){
			int e=rand()%5;
			if(e==0){
				int x=rand()%9+1;
				int y=rand()%9+1;
				swapNum(x,y);
			}else if(e==1){
				int t=rand()%3;
				int x=rand()%3+t*3;
				int y=rand()%3+t*3;
				swapRow(x,y);
			}else if(e==2){
				int t=rand()%3;
				int x=rand()%3+t*3;
				int y=rand()%3+t*3;
				swapCol(x,y);
			}else if(e==3){
				int x=rand()%4;
				rotate(x);
			}else if(e==4){
				int x=rand()%1;
				flip(x);
			}
		}
	 

	for(int i=0;i<rand()%50+30;++i){
		int x=rand()%9;
		int y=rand()%9;
		this->empty(x,y);
	}


	this->pSudoku();
}

void Sudoku::swapNum(int x,int y){
	/*x->y ; y->x*/
	for(int i=0;i<9;++i){
		for(int j=0;j<9;++j){
			if(sudoku[i][j]==x)
				sudoku[i][j]=y;
			else if(sudoku[i][j]==y)
				sudoku[i][j]=x;
		}
	}
}

void Sudoku::swapRow(int x,int y){
	for(int i=0;i<9;++i){
		int temp=sudoku[x][i];
		sudoku[x][i]=sudoku[y][i];
		sudoku[y][i]=temp;
	}
}

void Sudoku::swapCol(int x,int y){
	for(int i=0;i<9;i++){
		int temp=sudoku[i][x];
		sudoku[i][x]=sudoku[i][y];
		sudoku[i][y]=temp;
	}
}

void Sudoku::rotate(int x){
	for(int i=1;i<=x;++i){
		int s_copy[9][9];
		for(int i=0;i<9;++i){
			for(int j=0;j<9;++j)
				s_copy[i][j]=sudoku[i][j];
		}
	
		for(int j=8;j>=0;--j){
			for(int k=0;k<9;++k){
				sudoku[k][j]=s_copy[8-j][k];
			}
		}
	}
}

void Sudoku::flip(int x){
	if(x==0){
		//swapRow(0,8);....
		for(int i=0;i<4;++i){
			this->swapRow(i,8-i);
		}
	}else if(x==1){
		for(int i=0;i<4;++i){
			this->swapCol(i,8-i);
		}
	}
}

void Sudoku::empty(int x,int y){
	sudoku[x][y]=0;
}

void Sudoku::pSudoku(){
	for(int i=0;i<9;++i){
		for(int j=0;j<9;++j){
			cout<<sudoku[i][j];
			if(j!=8)
				cout<<" ";
			else
				cout<<endl;
		}
	}
}

void Sudoku::pUnique(){
	if(qty_s==1){
		for(int i=0;i<9;++i){
			for(int j=0;j<9;++j){
				cout<<unique[i][j];
				if(j!=8)
					cout<<" ";
				else
					cout<<endl;
			}
		}
	}else if(qty_s==0)
		cout<<"0"<<endl;
}

void Sudoku::next(int i,int j){
	j==8?solve(i+1,0):solve(i,j+1);
}


void Sudoku::solve(int x,int y){
	if(x==9){
		++qty_s;
		if(qty_s==2){
			cout<<"2"<<endl;
			exit(0);
		}
		for(int i=0;i<9;++i){
			for(int j=0;j<9;++j){
				unique[i][j]=sudoku[i][j];
			}
		}
		return;
	}
	if(sudoku[x][y]!=0){
		next(x,y);
		return;
	}else if(sudoku[x][y]==0){
		for(int k=1;k<=9;++k){
			if(can_fill(x,y,k)){
				sudoku[x][y]=k;
				next(x,y);//Solve the next box
				sudoku[x][y]=0;
			}					
		}
	}
	//return 0;
}


bool Sudoku::can_fill(int x,int y,int k){
	/*Row,Col,Cell*/
	return can_Row(x,k) && can_Col(y,k) && can_Cell(x,y,k);
}

bool Sudoku::can_Row(int x,int k){
	for(int i=0;i<9;++i){
		if(sudoku[x][i]==k)
			return false;
	}
	return true;
}

bool Sudoku::can_Col(int y,int k){
	for(int i=0;i<9;++i){
		if(sudoku[i][y]==k)
			return false;
	}
	return true;
}

bool Sudoku::can_Cell(int x,int y,int k){
	int cell_x=x/3*3;
	int cell_y=y/3*3;

	for(int i=cell_x;i<cell_x+3;++i){
		for(int j=cell_y;j<cell_y+3;++j){
			if(sudoku[i][j]==k)
				return false;
		}
	}

	return true;
}


