#include <iostream>
#include "sudoku.h"
using namespace std;

int main(){
	int ques[9][9];
	for(int i=0;i<9;++i){
		for(int j=0;j<9;++j){
			cin>>ques[i][j];
		}
	}

	Sudoku S(ques);
	S.set_s(ques);


	cout<<"Question:"<<endl;
	S.pSudoku();
	
	S.solve(0,0);

	cout<<"Answer:"<<endl;
	S.pUnique();
}
